const path = require("path");
const webpack = require('webpack');

module.exports = async ({
    config,
    mode
}) => {

    config.resolve.extensions.push('js');
    config.resolve.extensions.push('jsx');
    config.resolve.extensions.push('json');

    config.resolve.alias["assets"] = path.resolve(__dirname, '../assets/');
    config.resolve.alias["components"] = path.resolve(__dirname, '../src/components');
    config.resolve.alias["data"] = path.resolve(__dirname, '../src/data');
    config.resolve.alias["screens"] = path.resolve(__dirname, '../src/screens');

    return config;
};

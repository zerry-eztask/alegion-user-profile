import React from 'react';
import {configure, addDecorator} from '@storybook/react';
import {configureActions} from '@storybook/addon-actions';
import Theme from 'components/Theme';

import 'assets/css/base.css';

const req = require.context('./../src', true, /story\.js$/);

configureActions({clearOnStoryChange: true, limit: 20});

addDecorator((story) => <Theme>{story()}</Theme>);

function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

import sampleUserPhoto from 'assets/img/user-photo.jpg';

export default {
    contact_photo: sampleUserPhoto,
    contact_name: "Zerry Hogan",
    contact_email: "",
    contact_phone: "832-208-2797",
    contact_address: "123 Hired Lane",
    contact_city: "Austin",
    contact_state: "TX",
    contact_zipCode: "",
}

export const overallStats = [{
        meter: {
            values: [{
                value: 82,
                label: "Overall Score",
                color: "#00C781"
            }]
        },
        value: "82",
        name: "Overall Score"
    },
    {
        meter: {
            values: [{
                value: 90,
                label: "Completion Rate",
                color: "#00C781"
            }]
        },
        value: "90%",
        name: "Completion Rate"
    },
    {
        meter: {
            values: [{
                value: 95,
                label: "Accuracy Rate",
                color: "#00C781"
            }]
        },
        value: "95%",
        name: "Accuracy Rate"
    },
    {
        meter: {
            values: [{
                value: 2,
                label: "Incompletion Rate",
                color: "#00C781"
            }]
        },
        value: "2%",
        name: "Incompletion Rate"
    },
    {
        meter: {
            values: [{
                value: 5,
                label: "Failure Rate",
                color: "#00C781"
            }]
        },
        value: "5%",
        name: "Failure Rate"
    }
];

export const weekStats = [{
        meter: {
            values: [{
                value: (22 / 30) * 100,
                label: "Tasks Submitted",
                color: "#7D4CDB"
            }]
        },
        value: "22/30",
        name: "Tasks Submitted"
    },
    {
        meter: {
            values: [{
                value: (20 / 22) * 100,
                label: "Tasks Completed",
                color: "#7D4CDB"
            }]
        },
        value: "20/22",
        name: "Tasks Completed"
    },
    {
        meter: {
            values: [{
                value: (2349 / 2569) * 100,
                label: "Potential Earnings",
                color: "#7D4CDB"
            }]
        },
        value: "$2349/$2569",
        name: "Potential Earnings"
    },
    {
        meter: {
            values: [{
                value: 100,
                label: "Money Earned",
                color: "#7D4CDB"
            }]
        },
        value: "$1503",
        name: "Money Earned"
    }
];

export const skillsOverviewStats = [{
        meter: {
            values: [{
                value: (20 / 22) * 100,
                label: "Weighted Proficiency",
                color: "#333333"
            }]
        },
        value: "78",
        name: "Weighted Proficiency",
        title: "This number represents the weighted average of all the skills you are proficient in."
    },
    {
        meter: {
            values: [{
                value: 70,
                label: "Average Proficiency",
                color: "#333333"
            }]
        },
        value: "70",
        name: "Average Proficiency",
        title: "This number represents the average level of all of the skills you are proficient in."
    }, {
        meter: {
            values: [{
                value: (15 / 30) * 100,
                label: "Total Skills Aquired",
                color: "#333333"
            }]
        },
        value: "15/30",
        name: "Total Skills Aquired",
        title: "This number represents the number of skills you are proficient in."
    },
    {
        meter: {
            values: [{
                value: 60,
                label: "Top Skills Acquired",
                color: "#333333"
            }]
        },
        value: "6/10",
        name: "Top Skills Acquired",
        title: "This number represents the number of top skills you are proficient in."
    },
    {
        meter: {
            values: [{
                value: (9 / 20) * 100,
                label: "Secondary Skills Acquired",
                color: "#333333"
            }]
        },
        value: "9/20",
        name: "Secondary Skills Acquired",
        title: "This number represents the number of secondary skills you are proficient in."
    }
];

export const topSkillsStats = [{
        meter: {
            values: [{
                value: 96,
                label: "JavaScript",
                color: "#00C781"
            }]
        },
        value: "96",
        name: "JavaScript"
    },
    {
        meter: {
            values: [{
                value: 95,
                label: "HTML5",
                color: "#00C781"
            }]
        },
        value: "95",
        name: "HTML5"
    },
    {
        meter: {
            values: [{
                value: 87,
                label: "CSS",
                color: "#00C781"
            }]
        },
        value: "87",
        name: "CSS"
    },
    {
        meter: {
            values: [{
                value: 85,
                label: "ReactJS",
                color: "#00C781"
            }]
        },
        value: "85",
        name: "ReactJS"
    },
    {
        meter: {
            values: [{
                value: 84,
                label: "User Experience",
                color: "#00C781"
            }]
        },
        value: "84",
        name: "User Experience"
    }
];

export const skillsNeededStats = [{
        meter: {
            values: [{
                value: 15,
                label: "Humour",
                color: "#FF4040"
            }]
        },
        value: "15",
        name: "Humour"
    },
    {
        meter: {
            values: [{
                value: 22,
                label: "Machine Learning",
                color: "#FF4040"
            }]
        },
        value: "22",
        name: "Machine Learning"
    },
    {
        meter: {
            values: [{
                value: 49,
                label: "React Native",
                color: "#FF4040"
            }]
        },
        value: "49",
        name: "React Native"
    },
    {
        meter: {
            values: [{
                value: 52,
                label: "Mobile Development",
                color: "#FF4040"
            }]
        },
        value: "52",
        name: "Mobile Development"
    },
    {
        meter: {
            values: [{
                value: 80,
                label: "Object-Oriented Programming",
                color: "#FF4040"
            }]
        },
        value: "80",
        name: "Object-Oriented Programming"
    }
];

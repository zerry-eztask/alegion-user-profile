export default [{
    id: "1",
    title: "Task #1",
    description: "Refactor theme styling component.",
    status: "In Progress",
    time: "2m"
}, {
    id: "2",
    title: "Task #2",
    description: "Improve time complexity of component.",
    status: "Completed",
    time: "2m"
}, {
    id: "3",
    title: "Task #3",
    description: "Finish documenting base components.",
    status: "Failed",
    time: "2m"
}, {
    id: "4",
    title: "Task #4",
    description: "Rethink folder structure for apis.",
    status: "New",
    time: "2m"
}];

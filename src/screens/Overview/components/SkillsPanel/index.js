import React from 'react';
import PropTypes from 'prop-types';
import Panel from 'components/Panel';
import Section from './components/Section';
import {Box, Button} from 'grommet';

/**
    * The Skills Panel for the Overview Page
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const SkillsPanel = (props) => {

    const {
        overview,
        topSkills,
        skillsNeeded
    } = props;

    const flex = {
        grow: 1,
        shrink: 1
    };

    return (
        <Panel name="Skills Overview" a11yTitle="A list of your top 10 skills." flex={{grow: 3, shrink: 1}} basis="0" basis="0">
            <Box direction="row" align="stretch" justify="evenly">
                <Section
                    title="Overview"
                    description="This is a summary of your primary and secdonary skills."
                    stats={overview}
                />
                <Section
                    title="Top Skills"
                    description="This is an overview of the primary skills you perform highest on."
                    stats={topSkills}
                />
                <Section
                    title="Lowest Skills"
                    description="This is an overview of the primary skills you perform lowest on."
                    stats={skillsNeeded}
                />
            </Box>
            <Box direction="row" justify="start" margin={{top: "15px"}}>
                <Button type="button" label="View Full Report"/>
            </Box>
        </Panel>
    );

};

SkillsPanel.propTypes = {
    overview: PropTypes.array.isRequired,
    topSkills: PropTypes.array.isRequired,
    skillsNeeded: PropTypes.array.isRequired
};

export default SkillsPanel;

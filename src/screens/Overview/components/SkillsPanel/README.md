# Skills Panel

The Skill Panel for the User Profile Page. This panel shows the user what skills they are proficient in, what skills most need improvement to increase their perfoamnce abilities and then an overview of their skill proficiency.

## Props

Inherits props from `<Panel/>` Component.

| Prop         | Type  | Default | Required | Description                                                                                 |
| ------------ | ----- | ------- | -------- | ------------------------------------------------------------------------------------------- |
| overview     | array |         | true     | An array of objects that will be rendered as a `<Stat/>` for the **Overview Section**.      |
| topSkills    | array |         | true     | An array of objects that will be rendered as a `<Stat/>` for the **Top Skills Section**.    |
| skillsNeeded | array |         | true     | An array of objects that will be rendered as a `<Stat/>` for the **Lowest Skills Section**. |

## Example Usage

    import SkillsPanel from './components/SkillsPanel';

    const data = {
        overview: [],
        topSkills: [],
        skillsNeeded:[]
    };

    <SkillsPanel overview={data.overview} topSkills={data.topSkills} skillsNeeded={data.skillsNeeded}/>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/screens/Overview/components/SkillsPanel)

## Authors

-   **Zerry Hogan** - _Client_

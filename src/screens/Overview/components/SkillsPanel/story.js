import React from 'react';
import ReadMe from './README.md';
import Skills from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import {skillsOverviewStats, topSkillsStats, skillsNeededStats} from 'data/stats';

const data = {
    overview: skillsOverviewStats,
    topSkills: topSkillsStats,
    skillsNeeded: skillsNeededStats
};

const story = storiesOf('Screens | Overview/Panels', module);

story.addDecorator(withReadme(ReadMe));

story.add('Skills Panel', () => <Skills {...data}/>);

import React from 'react';
import PropTypes from 'prop-types';
import Stat from 'components/Stat';
import {Box, Heading, Text} from 'grommet';

const Section = (props) => {

    const {title, description, stats} = props;

    const flex = {
        grow: 1,
        shrink: 1
    };

    return (
        <Box pad={{right: "25px"}} flex={flex} basis="0">
            <Heading level={5}>{title}</Heading>
            <Text as="div" size="13px" margin={{bottom: "15px"}} style={{fontStyle: "italic"}}>{description}</Text>
            <Box direction="column" justify="evenly">
                {stats.map((data, key) => <Stat key={key} type="bar" {...data}/>)}
            </Box>
        </Box>
    );
};

Section.displayName = "SkillsPanelSection";

Section.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    stats: PropTypes.array.isRequired
};

export default Section;

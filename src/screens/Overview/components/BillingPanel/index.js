import React from 'react';
import PropTypes from 'prop-types';
import Panel, {WithForm} from 'components/Panel';
import ListForm from 'components/ListForm';
import {Box} from 'grommet';

/**
    * The Billing Panel for the Overview Page
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const BillingPanel = props => {
    const {
        data,
        onFormSubmit,
        onFieldChange,
        editing,
        unsetFormEditMode,
        setFormEditMode
    } = props;

    const {card_number, card_expiration, card_ccv, card_zip_code} = data;

    return (
        <Panel name="Billing Information" a11yTitle="A summary of your billing information." height={350}>
            <ListForm onSubmit={onFormSubmit} editing={editing} labelWidth="120px">
                <ListForm.Input label="Card Number" htmlFor="card_number" placeholder="Enter Card Number" type={editing ? "text" : "password"} value={card_number} onChange={(e) => onFieldChange("card_number", e.target.value)}/>
                <ListForm.Input label="Card Exipration" htmlFor="card_expiration" placeholder="Enter Exipration Date" value={card_expiration} onChange={(e) => onFieldChange("card_expiration", e.target.value)}/>
                <ListForm.Input label="CCV Code" htmlFor="card_ccv" placeholder="Enter CCV Code" type={editing ? "text" : "password"} value={card_ccv} onChange={(e) => onFieldChange("card_ccv", e.target.value)}/>
                <ListForm.Input label="Zip Code" htmlFor="card_zip_code" placeholder="Enter Zip Code" value={card_zip_code} onChange={(e) => onFieldChange("card_zip_code", e.target.value)}/>
                <Box direction="row" align="center" justify="center" style={{marginTop: 10}}>
                    {!editing && <ListForm.Button type="button" label="Edit" margin="xsmall" onClick={setFormEditMode}/>}
                    {editing && <ListForm.Button type="button" label="Cancel" margin="xsmall" onClick={unsetFormEditMode}/>}
                    {editing && <ListForm.Button type="submit" label="Save" margin="xsmall"/>}
                </Box>
            </ListForm>
        </Panel>
    );
};

BillingPanel.defaultProps = {};

BillingPanel.propTypes = {
    data: PropTypes.shape({
        card_number: PropTypes.string,
        card_expiration: PropTypes.string,
        card_ccv: PropTypes.string,
        card_zip_code: PropTypes.string
    }).isRequired
};

export default WithForm(BillingPanel);

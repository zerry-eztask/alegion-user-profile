import React from 'react';
import ReadMe from './README.md';
import BillingPanel from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import billingInfo from 'data/billing';

const story = storiesOf('Screens | Overview/Panels', module);

story.addDecorator(withReadme(ReadMe));

story.add('Billing Panel', () => (
    <div style={{width: 400, margin: "0 auto"}}>
        <BillingPanel data={billingInfo} onFormSubmit={action("onFormSubmit")}/>
    </div>
));

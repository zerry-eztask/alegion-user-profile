# Billing Panel

The Billing Information Panel for the User Profile Page. Users are able to view and edit their billing information using this panel by clicking the "edit" button.

## Props

Inherits props from `<Panel/>` Component and uses the `WithForm()` HOC imported from the `<Panel/>` Component.

| Prop         | Type   | Default | Required | Description                                                                                                       |
| ------------ | ------ | ------- | -------- | ----------------------------------------------------------------------------------------------------------------- |
| data         | object |         | true     | The form data.                                                                                                    |
| onFormSubmit | string |         |          | A function to be called when the user has updated their billing information by submitting the form in this panel. |

## Form Data Structure

The structure of the form object prop is demonstrated below.

When the form is submitted, the onFormSubmit function will be called and the serialized form data will be passed as the only argument.

    {
        card_number:string,
        card_expiration:string,
        card_ccv:string,
        card_zip_code:string
    }

## Example Usage

    import BillingPanel from './components/BillingPanel';

    const data = {
        card_number: "1234-7980-9080-4567",
        card_expiration: "08/2022",
        card_ccv: "555",
        card_zip_code: "77081"
    };

    <BillingPanel data={data} onFormSubmit={someFunction}/>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/screens/Overview/components/BillingPanel/)

## Authors

-   **Zerry Hogan** - _Client_

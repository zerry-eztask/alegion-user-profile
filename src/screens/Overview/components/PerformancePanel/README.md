# Performance Panel

The Performance Panel for the User Profile Page. The panel allows the user to see how the system is tracking and rating their performance.

## Props

Inherits props from `<Panel/>` Component.

| Prop  | Type  | Default | Required | Description                                              |
| ----- | ----- | ------- | -------- | -------------------------------------------------------- |
| stats | array |         | true     | An array of objects that will be rendered as a `<Stat/>` |

## Example Usage

    import PerformancePanel from './components/PerformancePanel';

    const stats = [{
        meter: {
            values: [{
                value: 82,
                label: "Overall Score"
            }]
        },
        value: "82",
        name: "Overall Score"
    }];

    <PerformancePanel stats={stats}/>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/screens/Overview/components/PerformancePanel)

## Authors

-   **Zerry Hogan** - _Client_

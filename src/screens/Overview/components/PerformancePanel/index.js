import React from 'react';
import PropTypes from 'prop-types';
import Panel from 'components/Panel';
import Stat from 'components/Stat';
import {Box} from 'grommet';

/**
    * The Contact Panel for the Overview Page
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const PerformancePanel = (props) => {

    const {
        onClick,
        stats
    } = props;

    const flex = {
        grow: 1
    };

    const contentProps = {
        justify: "center"
    };

    return (
        <Panel name="My Performance" a11yTitle="A summary of your performance." height={350} onClick={() => {}} contentProps={contentProps}>
            <Box direction="row" justify="evenly">
                {stats.map((stat, key) => <Stat key={key} margin="xsmall" {...stat}/>)}
            </Box>
        </Panel>
    );

};

PerformancePanel.defaultProps = {};

PerformancePanel.propTypes = {
    stats: PropTypes.array.isRequired
};

export default PerformancePanel;

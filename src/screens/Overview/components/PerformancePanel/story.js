import React from 'react';
import ReadMe from './README.md';
import PerformancePanel from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import {overallStats} from 'data/stats';

const story = storiesOf('Screens | Overview/Panels', module);

story.addDecorator(withReadme(ReadMe));

story.add('Performance Panel', () => <PerformancePanel stats={overallStats}/>);

import React from 'react';
import PropTypes from 'prop-types';
import Panel from 'components/Panel';
import TaskList from 'components/TaskList';
import Stat from 'components/Stat';
import {Box, Button, Tabs, Tab} from 'grommet';

/**
    * The Contact Panel for the Overview Page
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const TasksOverviewPanel = (props) => {

    const {
        tasks,
        stats
    } = props;

    const flex = {
        grow: 1
    };

    const Stats = () => (
        <Box direction="column">
            {stats.map((data, key) => <Stat key={key} type="bar" {...data}/>)}
        </Box>
    );

    return (
        <Panel name="Task Overview for This Week" a11yTitle="A list of your pending tasks to complete." flex={{grow: 1, shrink: 1}} basis="0" height={690}>
            <Tabs margin={{bottom: "15px"}}>
                <Tab title="Statistics">
                    <Stats/>
                    <Box direction="row" align="center" justify="center" margin={{top: "15px"}}>
                        <Button type="button" label="View Full Overview"/>
                    </Box>
                </Tab>
                <Tab title="Recent">
                    <TaskList tasks={tasks}/>
                    <Box direction="row" align="center" justify="center" margin={{top: "15px"}}>
                        <Button type="button" label="View All Tasks"/>
                    </Box>
                </Tab>
            </Tabs>
        </Panel>
    );

};

TasksOverviewPanel.defaultProps = {};

TasksOverviewPanel.propTypes = {
    stats: PropTypes.array.isRequired,
    tasks: PropTypes.array.isRequired
};

export default TasksOverviewPanel;

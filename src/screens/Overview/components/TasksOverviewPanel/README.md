# This Weeks Overview Panel

The Weekly Overview Panel for the User Profile Page. This panel shows the user statistics related to their weekly progress for the tasks they have been assigned.
They can use this panel to keep track of how much money their going to earn and how much money is possible for them to earn.

## Props

Inherits props from `<Panel/>` Component.

| Prop  | Type  | Default | Required | Description                                                    |
| ----- | ----- | ------- | -------- | -------------------------------------------------------------- |
| stats | array |         | true     | An array of objects that will be rendered as a `<Stat/>`       |
| tasks | array |         | true     | An array of objects that will be rendered into a `<TaskList/>` |

## Example Usage

    import TasksOverviewPanel from './components/TasksOverviewPanel';

    const stats = [{
        meter: {
            values: [{
                value: 82,
                label: "Overall Score"
            }]
        },
        value: "82",
        name: "Overall Score"
    }];

    const tasks = [{
        id: "3",
        title: "Task #3",
        description: "Finish documenting base components.",
        status: "Failed",
        time: "2m"
    }];

    <TasksOverviewPanel stats={stats} tasks={tasks}/>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/screens/Overview/components/TasksOverviewPanel)

## Authors

-   **Zerry Hogan** - _Client_

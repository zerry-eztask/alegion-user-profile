import React from 'react';
import ReadMe from './README.md';
import TasksOverviewPanel from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import {weekStats} from 'data/stats';
import tasks from 'data/tasks';

const story = storiesOf('Screens | Overview/Panels', module);

story.addDecorator(withReadme(ReadMe));

story.add('Task Overview Panel', () => (
    <div style={{width: 400, margin: "0 auto"}}>
        <TasksOverviewPanel stats={weekStats} tasks={tasks}/>
    </div>
));

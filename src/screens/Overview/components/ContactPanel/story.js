import React from 'react';
import ReadMe from './README.md';
import ContactPanel from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import contactInfo from 'data/contact';

const story = storiesOf('Screens | Overview/Panels', module);

story.addDecorator(withReadme(ReadMe));

story.add('Contact Panel', () => (
    <div style={{width: 400, margin: "0 auto"}}>
        <ContactPanel data={contactInfo} onFormSubmit={action("onFormSubmit")}/>
    </div>
));

import React from 'react';
import PropTypes from 'prop-types';
import Panel, {WithForm} from 'components/Panel';
import ListForm from 'components/ListForm';
import UserPhoto from 'components/UserPhoto';
import {Box} from 'grommet';

/**
    * The Contact Panel for the Overview Page
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const ContactPanel = props => {
    const {
        data,
        onFormSubmit,
        onFieldChange,
        editing,
        unsetFormEditMode,
        setFormEditMode
    } = props;

    const {
        contact_photo,
        contact_name,
        contact_email,
        contact_phone,
        contact_address,
        contact_city,
        contact_state,
        contact_zipCode
    } = data;

    return (
        <Panel a11yTitle="A summary of your contact information." height={600}>
            <ListForm onSubmit={onFormSubmit} editing={editing} labelWidth="90px">
                <Box align="center" margin={{
                        "vertical" : "25px"
                    }}>
                    <UserPhoto src={contact_photo} alt={`${contact_name}\'s Profile Photo`} width={100} height={100}/>
                </Box>
                <ListForm.Input label="Full Name" htmlFor="contact_name" placeholder="Enter Full Name" value={contact_name} onChange={(e) => onFieldChange("contact_name", e.target.value)}/>
                <ListForm.Input label="Cell Phone" htmlFor="contact_phone" placeholder="Enter Cell Phone" value={contact_phone} onChange={(e) => onFieldChange("contact_phone", e.target.value)}/>
                <ListForm.Input label="Email" htmlFor="contact_email" placeholder="Enter Email" value={contact_email} onChange={(e) => onFieldChange("contact_email", e.target.value)}/>
                <ListForm.Input label="Address" htmlFor="contact_address" placeholder="Enter Address" value={contact_address} onChange={(e) => onFieldChange("contact_address", e.target.value)}/>
                <ListForm.Input label="City" htmlFor="contact_city" placeholder="Enter City" value={contact_city} onChange={(e) => onFieldChange("contact_city", e.target.value)}/>
                <ListForm.Input label="State" htmlFor="contact_state" placeholder="Enter State" value={contact_state} onChange={(e) => onFieldChange("contact_state", e.target.value)}/>
                <ListForm.Input label="Zip Code" htmlFor="contact_zipCode" placeholder="Enter Zip Code" value={contact_zipCode} onChange={(e) => onFieldChange("contact_zipCode", e.target.value)}/>
                <Box direction="row" align="center" justify="center" style={{
                        marginTop: 10
                    }}>
                    {!editing && <ListForm.Button type="button" label="Edit" margin="xsmall" onClick={setFormEditMode}/>}
                    {editing && <ListForm.Button type="button" label="Cancel" margin="xsmall" onClick={unsetFormEditMode}/>}
                    {editing && <ListForm.Button type="submit" label="Save" margin="xsmall"/>}
                </Box>
            </ListForm>
        </Panel>
    );
};

ContactPanel.defaultProps = {
    onFormSubmit: (data) => {}
};

ContactPanel.propTypes = {
    data: PropTypes.shape({
        contact_photo:PropTypes.string,
        contact_name:PropTypes.string,
        contact_email:PropTypes.string,
        contact_phone:PropTypes.string,
        contact_address:PropTypes.string,
        contact_city:PropTypes.string,
        contact_state:PropTypes.string,
        contact_zipCode:PropTypes.string
    }).isRequired,
    onFormSubmit: PropTypes.func
};

export default WithForm(ContactPanel);

# Contact Panel

The Contact Information Panel for the User Profile Page. Users are able to view and edit their contact information using this panel by clicking the "edit" button.

## Props

Inherits props from `<Panel/>` Component and uses the `WithForm()` HOC imported from the `<Panel/>` Component.

| Prop         | Type   | Default      | Required | Description                                                                               |
| ------------ | ------ | ------------ | -------- | ----------------------------------------------------------------------------------------- |
| data         | object |              | true     | The form data.                                                                            |
| onFormSubmit | func   | (data) => {} |          | A function to be called when the user has submitted the form to update their information. |

## Form Data Structure

The structure of the form object prop is demonstrated below.

When the form is submitted, the onFormSubmit function will be called and the serialized form data will be passed as the only argument.

    {
        contact_address:string,
        contact_city:string,
        contact_email:string,
        contact_name:string,
        contact_phone:string,
        contact_photo:string,
        contact_state:string,
        contact_zipCode:string,
    }

## Example Usage

    import ContactPanel from './components/ContactPanel';

    const data = {
        contact_photo: sampleUserPhoto,
        contact_name: "Zerry Hogan",
        contact_email: "",
        contact_phone: "832-208-2797",
        contact_address: "123 Hired Lane",
        contact_city: "Austin",
        contact_state: "TX",
        contact_zipCode: "",
    };

    <ContactPanel data={data} onFormSubmit={someFunction}/>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/screens/Overview/components/ContactPanel)

## Authors

-   **Zerry Hogan** - _Client_

# Overview Screen

The screen provides an overview of the users personal information and also their performance and tasks information.

This screen has several Event props that are fired during interactivity with the UI. You can plug into these events to take actions as needed.

For example, you can pass a function into the **_onContactInformationChanged_** prop that makes a POST request to save the users contact information whenever the user updates their contact information.

## Props

| Prop                        | Type     | Default                                           | Required | Description                                                                          |
| --------------------------- | -------- | ------------------------------------------------- | -------- | ------------------------------------------------------------------------------------ |
| initialState                | object   | {contact, billing, performance, overview, skills} |          | An object that contacts the initial state information for each panel.                |
| onContactInformationChanged | function | null                                              |          | A function that will be called after the user has updated their contact information. |
| onBillingInformationChanged | function | null                                              |          | A function that will be called after the user has updated their billing information. |

## Example Usage

    import Overview from 'app/src/screens/Overview';

    const initialState = {
        contact: {
            data: {}
        },
        billing: {
            data: {}
        },
        performance: {
            stats: []
        },
        overview: {
            tasks: [],
            stats: []
        },
        skills: {
            overview: [],
            topSkills: [],
            skillsNeeded: []
        }
    };

    <Overview
        initialState={initialState}
        onContactInformationChanged={(info) => {}}
        onBillingInformationChanged={(info) => {}}
    />

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/screens/Overview/)

## Authors

-   **Zerry Hogan** - _Client_

import React from 'react';
import ReadMe from './README.md';
import Overview from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import sampleUserPhoto from 'assets/img/user-photo.jpg';
import {overallStats, weekStats, skillsOverviewStats, topSkillsStats, skillsNeededStats} from 'data/stats';
import tasks from 'data/tasks';
import billingInfo from 'data/billing';
import contactInfo from 'data/contact';

const story = storiesOf('Screens | Overview', module);

const initialState = {
    contact: {
        data: contactInfo
    },
    billing: {
        data: billingInfo
    },
    performance: {
        stats: overallStats
    },
    overview: {
        stats: weekStats,
        tasks: tasks
    },
    skills: {
        overview: skillsOverviewStats,
        topSkills: topSkillsStats,
        skillsNeeded: skillsNeededStats
    }
};

story.addParameters({});

story.addDecorator(withReadme(ReadMe));

story.add('Screen', () => <Overview
                            initialState={initialState}
                            onContactPanelClicked={action("onContactPanelClicked")}
                            onBillingPanelClicked={action("onBillingPanelClicked")}
                            onPerformancePanelClicked={action("onPerformancePanelClicked")}
                            onTasksOverviewPanelClicked={action("onTasksOverviewPanelClicked")}
                            onTaskAccuracyPanelClicked={action("onTaskAccuracyPanelClicked")}
                            onSkillsPanelClicked={action("onSkillsPanelClicked")}
                            onContactInformationChanged={action("onContactInformationChanged")}
                            onBillingInformationChanged={action("onBillingInformationChanged")}
                            />);

import React from 'react';
import PropTypes from 'prop-types';
import {Box} from 'grommet';
import ContactPanel from './components/ContactPanel';
import BillingPanel from './components/BillingPanel';
import PerformancePanel from './components/PerformancePanel';
import TasksOverviewPanel from './components/TasksOverviewPanel';
import SkillsPanel from './components/SkillsPanel';

import './style.css';

/**
    * The overview screen for the user dashboard. Shows an initial summary of the users information.
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Overview = (props) => {

    const {
        initialState,
        onContactInformationChanged,
        onBillingInformationChanged
    } = props;

    return (
        <div id="screen-overview">
            <Box direction="row" wrap={true}>
                <Box direction="column" flex={{grow: 1, shrink: 1}} basis="0">
                    <ContactPanel onFormSubmit={onContactInformationChanged} {...initialState.contact}/>
                    <BillingPanel onFormSubmit={onBillingInformationChanged} {...initialState.billing}/>
                </Box>
                <Box direction="column" flex={{grow: 4, shrink: 1}} basis="0">
                    <PerformancePanel {...initialState.performance}/>
                    <Box direction="row">
                        <TasksOverviewPanel {...initialState.overview}/>
                        <SkillsPanel {...initialState.skills}/>
                    </Box>
                </Box>
            </Box>
        </div>
    );

};

Overview.displayName = "Screen(Overview)";

Overview.defaultProps = {
    initialState: {
        contact: {},
        billing: {},
        performance: {},
        overview: {},
        skills: {}
    }
};

Overview.propTypes = {
    initialState: PropTypes.shape({
        contact: PropTypes.object.isRequired,
        billing: PropTypes.object.isRequired,
        performance: PropTypes.object.isRequired,
        overview: PropTypes.object.isRequired,
        skills: PropTypes.object.isRequired
    })
};

export default Overview;

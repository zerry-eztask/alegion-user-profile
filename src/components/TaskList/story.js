import React from 'react';
import ReadMe from './README.md';
import TaskList from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';
import tasks from 'data/tasks';

const story = storiesOf('Components | TaskList', module);

story.addParameters({});

story.addDecorator(withReadme(ReadMe));

story.add('Default', () => (
    <div style={{width: 400, margin: "0 auto"}}>
        <TaskList tasks={tasks}/>
    </div>
));

# Task List

A component that displays a list of tasks.

## TaskList Props

The container component.

| Prop  | Type  | Default | Required | Description        |
| ----- | ----- | ------- | -------- | ------------------ |
| tasks | array |         | true     | An array of tasks. |

## Task Props

The individual task component.

| Prop        | Type                                              | Default | Required | Description                                       |
| ----------- | ------------------------------------------------- | ------- | -------- | ------------------------------------------------- |
| id          | string                                            |         | true     | The unique identifier for the task.               |
| title       | string                                            |         | true     | The title of the task.                            |
| description | string                                            |         | true     | The description of the task.                      |
| status      | enum("Completed", "In Progress", "Failed", "New") |         | true     | The status of the task.                           |
| id          | string                                            |         | true     | A function to be called when the task is clicked. |

## Example Usage

    import TaskList from 'components/TaskList';

    const tasks = [{
        id: "1",
        title: "Task #1",
        description: "Take out the trash but don't forget to take it to dumpster #478 for proper decomposition.",
        status: "In Progress",
        time: "2m"
    }, {
        id: "2",
        title: "Task #2",
        description: "Take out the trash but don't forget to take it to dumpster #478 for proper decomposition.",
        status: "Completed",
        time: "2m"
    }];

    <TaskList tasks={tasks}/>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/components/TaskList/)

## Authors

-   **Zerry Hogan** - _Client_

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Box} from 'grommet';
import Task from './components/Task';

import './style.css';

/**
    * A List of Tasks
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const TaskList = (props) => {

    const {children, tasks} = props;

    const classConfig = {
        "task-list": true
    };

    const className = classNames(classConfig);

    return (
        <Box className={className} as="ul">
            {tasks.map((task) => <Task key={task.id} {...task}/>)}
        </Box>
    );

};

TaskList.displayName = "TaskList";

TaskList.defaultProps = {
};

TaskList.propTypes = {
    tasks: PropTypes.array.isRequired
};

export default TaskList;

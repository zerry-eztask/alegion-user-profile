import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Box} from 'grommet';

/**
    * A Task Item
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Task = (props) => {

    const {children, title, description, status, time, onClick} = props;

    const _status = status.toLowerCase().replace(/\W/,"-");

    const classConfig = {
        "task": true,
        [`task--${_status}`]: true
    };

    const className = classNames(classConfig);

    return (
        <Box className={className} as="li" onClick={onClick}>
            <Box className="task__header" direction="row" align="center" justify="between">
                <div className="task__title">{title}</div>
                <div className="task__status">{status}</div>
            </Box>
            <div className="task__description">{description}</div>
            <div className="task__footer">{time}</div>
        </Box>
    );

};

Task.displayName = "Task";

Task.defaultProps = {
    completed: false,
    failed: false,
    onClick: (e) => {}
};

Task.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    status: PropTypes.oneOf(["Completed", "In Progress", "Failed", "New"]).isRequired,
    onClick: PropTypes.func
};

export default Task;

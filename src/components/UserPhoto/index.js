import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './style.css';

/**
    * A user Photo
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const UserPhoto = (props) => {

    const {src, alt, width, height, rounded} = props;

    const classConfig = {
        "user-photo": true,
        "user-photo--rounded": rounded
    };

    const className = classNames(classConfig);

    const style = {
        width: width,
        height: height
    };

    return (
        <div className={className} style={style}>
            <img src={src} alt={alt}/>
        </div>
    );

};

UserPhoto.displayName = "UserPhoto";

UserPhoto.defaultProps = {
    rounded: true
};

UserPhoto.propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    rounded: PropTypes.bool
};

export default UserPhoto;

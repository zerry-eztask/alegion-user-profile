# Stat

A circular meter used for displaying numerical statistics

## Props

All other props apply to the `<Box/>` Component that wraps the Stat. You can refer [here](https://v2.grommet.io/box) for the full documentation of their component.

| Prop        | Type                  | Default | Required | Description                                                                                                                |
| ----------- | --------------------- | ------- | -------- | -------------------------------------------------------------------------------------------------------------------------- |
| type        | enum("circle", "bar") | circle  |          | The type of stat to render.                                                                                                |
| meter       | object                |         | true     | An object of properties to apply to the Grommet Meter. Refer [here](https://v2.grommet.io/meter) for available properties. |
| value       | string                |         | true     | A string representation of the value of the meter such as "75%".                                                           |
| name        | string                |         | true     | A friendly name that informs the user what the value represents.                                                           |
| description | string                | ""      |          | A description that will be placed below the meter to provide additional information to the user about the stat.            |

## Example Usage

    import Stat from 'components/Stat';

    <Stat
        meter={{
            values: [{value: 75, label: "Work Done"}]
        }}
        value="75hrs"
        name="Work Done"
    />

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/components/Stat/)

## Authors

-   **Zerry Hogan** - _Client_

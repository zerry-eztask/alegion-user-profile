import React from 'react';
import PropTypes from 'prop-types';
import Circle from './components/Circle';
import Bar from './components/Bar';

import './style.css';

/**
    * A stat for displaying performance information
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Stat = (props) => {
    if (props.type == "circle") {
        return <Circle {...props}/>
    }
    if (props.type == "bar") {
        return <Bar {...props}/>
    }
};

Stat.displayName = "Stat";

Stat.defaultProps = {
    description: "",
    type: "circle",
    title: ""
};

Stat.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    meter: PropTypes.shape({
        values: PropTypes.array.isRequired
    }).isRequired,
    title: PropTypes.string,
    description: PropTypes.string,
    type: PropTypes.oneOf(["circle", "bar"])
};

export default Stat;

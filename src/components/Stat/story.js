import React from 'react';
import ReadMe from './README.md';
import Stat from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';

const story = storiesOf('Components | Stat', module);

story.addParameters({});

story.addDecorator(withReadme(ReadMe));

story.add('Circle', () => (
    <div>
        <Stat
        meter={{
            values: [{value: 75, label: "Work Done"}]
        }}
        value="75hrs"
        name="Work Done"/>
    </div>
));

story.add('Bar', () => (
    <div style={{width: 300, margin: "0 auto"}}>
        <Stat
        type="bar"
        meter={{
            values: [{value: 75, label: "Work Done"}]
        }}
        value="75%"
        name="Work Done"/>
    </div>
));

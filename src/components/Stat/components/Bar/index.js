import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Box, Meter} from 'grommet';

import './style.css';

/**
    * A stat for displaying performance information
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const BarStat = (props) => {

    const {meter, name, value, description, title, type, ...restProps} = props;

    const size = meter.size || "small";

    const thickness = meter.thickness || "small";

    const classConfig = {
        "stat": true,
        "stat--bar": true,
        [`stat--size-${size}`]: true,
        [`stat--thickness-${size}`]: true
    };

    const className = classNames(classConfig);

    return (
        <Box direction="column" align="center" justify="center" className={className} {...restProps} title={title}>
            <Box direction="row" justify="between" className="stat__container">
                <div className="stat__info__title">{name}</div>
                <div className="stat__info__value">{`${value}`}</div>
            </Box>
            <Meter {...meter} size={size} thickness={thickness} className="stat__meter" type="bar" />
        </Box>
    );

};

BarStat.displayName = "BarStat";

export default BarStat;

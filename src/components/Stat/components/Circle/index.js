import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Box, Meter} from 'grommet';

import './style.css';

/**
    * A stat for displaying performance information
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const CircleStat = (props) => {

    const {meter, name, value, description, title, type, ...restProps} = props;

    const size = meter.size || "small";

    const thickness = meter.thickness || "small";

    const classConfig = {
        "stat": true,
        "stat--circle": true,
        [`stat--size-${size}`]: true,
        [`stat--thickness-${size}`]: true
    };

    const className = classNames(classConfig);

    return (
        <Box direction="column" align="center" justify="center" className={className} {...restProps} title={title}>
            <Box direction="column" align="center" justify="center" className="stat__container">
                <Meter {...meter} size={size} thickness={thickness} className="stat__meter" type="circle" />
                <Box className="stat__info" align="center" justify="center">
                    <div className="stat__info__value">{`${value}`}</div>
                    <div className="stat__info__title">{name}</div>
                </Box>
            </Box>
            <Box className="stat__description" align="center" justify="center">
                {description}
            </Box>
        </Box>
    );

};

CircleStat.displayName = "CircleStat";

export default CircleStat;

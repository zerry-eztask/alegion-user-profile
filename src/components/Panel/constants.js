export const FLEX = {
    "auto": {
        flex: {
            grow: 1,
            shrink: 1
        },
        basis: "auto"
    },
    "initial": {
        flex: {
            grow: 0,
            shrink: 1
        },
        basis: "auto"
    },
    "none": {}
};

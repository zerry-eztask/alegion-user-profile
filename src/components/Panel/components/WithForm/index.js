import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/*
 * A Higher-Order Component for panels that have forms. This Component automatically manages the state of the form
*/

export default function(Panel) {

    class WithForm extends React.Component {

        static propTypes = {
            data: PropTypes.object.isRequired,
            onFormSubmit: PropTypes.func.isRequired
        }

        static defaultProps = {}

        static displayName = `PanelWithForm(${Panel.displayName || Panel.name || "Panel"})`;

        state = {
            data: {
                ...this.props.data
            },
            editing: false
        }

        onFormSubmit = ({nativeEvent}) => {

            nativeEvent.preventDefault();

            this.props.onFormSubmit(this.state.data);

            this.unsetFormEditMode();

        }

        /*
         * Updates the state when a form field is changed.
        */
        onFieldChange = (name, value) => {
            this.setState({
                data: {
                    ...this.state.data,
                    [name]: value
                }
            });
        }

        setFormEditMode() {
            this.setState({
                editing: true
            });
        }

        unsetFormEditMode() {
            this.setState({
                editing: false
            });
        }

        render() {

            const {data, ...restProps} = this.props;

            return <Panel
                    {...restProps}
                    {...this.state}
                    setFormEditMode={this.setFormEditMode.bind(this)}
                    unsetFormEditMode={this.unsetFormEditMode.bind(this)}
                    onFormSubmit={this.onFormSubmit}
                    onFieldChange={this.onFieldChange}/>;

        }

    }

    return WithForm;

}

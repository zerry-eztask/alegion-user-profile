# Panel

Information panel for displaying a block on content.

## Props

| Prop         | Type                            | Default   | Required | Description                                                                                                                           |
| ------------ | ------------------------------- | --------- | -------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| name         | string                          | ""        |          | A friendly name to label the panel. This title will be visible to users.                                                              |
| a11yTitle    | string                          |           | true     | A short sentence that describes the purpose of the panel. Will be used by screen readers.                                             |
| onClick      | function                        | (e) => {} |          | A function to be called when the panel is clicked.                                                                                    |
| shortFlex    | enum("auto", "initial", "none") | "none"    |          | A short-hand preset for the css "flex" property. [Reference](https://www.w3schools.com/CSSref/css3_pr_flex.asp#midcontentadcontainer) |
| contentProps | object                          | {}        |          | An object of properties to apply to the panel content.                                                                                |

## Example Usage

    import Panel from 'app/src/components/Panel';

    <Panel name="Contact Information">
        {children go here}
    </Panel>

## WithForm HOC

The Panel Component exposes a Higher-Order Component called `WithForm` that can be imported as such `import Panel, {withForm} from 'components/Panel'`.

The purpose of this HOC is to make it easy to manage the state of a form in a panel. It provides helper functions that make it easy to create controlled form fields that update the state when they change.

### Props

| Prop         | Type   | Default | Required | Description                                         |
| ------------ | ------ | ------- | -------- | --------------------------------------------------- |
| data         | object |         | true     | The form data.                                      |
| onFormSubmit | func   |         | true     | A function to be called when the form is submitted. |

### API

`export default WithForm(Component);`

| Prop      | Type                      | Default | Required | Description                                          |
| --------- | ------------------------- | ------- | -------- | ---------------------------------------------------- |
| Component | Any Valid React Component |         | true     | The React Component that will be wrapped by the HOC. |

### Usage

    //MyPanel.js
    import Panel, {WithForm} from 'app/src/components/Panel';

    class MyPanel extends React.Component {
        render() {

            const {onFormSubmit, onFieldChange, setFormEditMode, unsetFormEditMode, editing, data} = this.props;

            const {field_name} = data; //Desstructure the form state to pass into the form fields

            return (
                <Panel name="Contact Information">
                    <form onSubmit={onFormSubmit}>
                        <input type="text" placeholder="Enter Address" value={field_name} onChange={(e) => onFieldChange("field_name", e.target.value)}/>
                    </form>
                </Panel>
            );

        }
    }

    export default WithForm(MyPanel);

    //AnotherComponent.js
    import MyPanel from 'components/MyPanel';

    ...
    <MyPanel data={data} onFormSubmit={(data) => console.log(data)}/>
    ...

### Props Passed To Panel Component

-   **onFormSubmit** -  function - _(e)_ - Called when the form is submitted.
-   **onFieldChange** - function - _(name, value)_ - Updates a form fields state.
-   **setFormEditMode** - function - Sets the editing property of the form state to true.
-   **unsetFormEditMode** - function - Sets the editing property of the form state to false.
-   **data** - object - An object that contains the values of the form fields.
-   **editing** - bool - Whether or not the form is being edited. Useful with forms where you have to click to edit the form data.

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/components/Panel/)

## Authors

-   **Zerry Hogan** - _Client_

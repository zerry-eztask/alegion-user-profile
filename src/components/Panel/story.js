import React from 'react';
import ReadMe from './README.md';
import Panel from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';

const story = storiesOf('Components | Panel', module);

story.addParameters({});

story.addDecorator(withReadme(ReadMe));

story.add('Default', () => <Panel
                            name="Contact Information"
                            a11yTitle="A summary of your contact information."
                            width={400}
                            height={400}
                            onClick={action("onClick")}
                            />);

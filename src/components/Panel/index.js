import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Box, Heading} from 'grommet';
import {FLEX} from './constants';
import WithForm from './components/WithForm';

import './style.css';

/**
    * A panel box for displaying sections of information
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Panel = (props) => {

    const {children, name, width, height, onClick, shortFlex, contentProps, ...restProps} = props;

    const classConfig = {
        "panel": true,
        "panel--clickable": onClick
    };

    const className = classNames(classConfig);

    const style = {
        width: width,
        height: height
    };

    const flex = FLEX[shortFlex];

    return (
        <Box direction="column" className={className} style={style} onClick={onClick} {...restProps} {...flex}>
            {name && <Box className="panel__name" justify="center" title={name}><Heading level={4}>{name}</Heading></Box>}
            <Box className="panel__content" flex={{grow: 1}} {...contentProps}>{children}</Box>
        </Box>
    );

};

Panel.displayName = "Panel";

Panel.defaultProps = {
    name: "",
    onClick: null,
    shortFlex: "none",
    contentProps: {}
};

Panel.propTypes = {
    name: PropTypes.string,
    a11yTitle: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    shortFlex: PropTypes.oneOf(["auto", "initial", "none"]),
    contentProps: PropTypes.object
};

export default Panel;

export {
    WithForm
}

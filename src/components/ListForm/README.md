# List Form

The List Form Component displays a form and its' inputs and labels as indiviual rows. Where the inputs label is on the left and the form input is on the right.

## ListForm Props

All other props will be applied to the `<form>` element.

| Prop       | Type   | Default | Required | Description                                                                                                                                     |
| ---------- | ------ | ------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| editing    | bool   | false   |          | A boolean that indicates whether or not the form is currently being edited. If set to true, the UI of the Form will present itself as editable. |
| labelWidth | string |         | true     | The width of the label elements on the left, can be any valid css width such as "75px" or "10%".                                                |

## ListForm.Input Props

Any other props passed besides the ones listed below will only be passed onto the input element.

| Prop        | Type     | Default | Required | Description                                         |
| ----------- | -------- | ------- | -------- | --------------------------------------------------- |
| label       | string   |         | true     | The text for the label tag.                         |
| htmlFor     | string   |         | true     | The name of the input.                              |
| placeholder | function |         | true     | The placeholder text to show if the input is input. |

## ListForm.Button Props

Refer to the documentation for a [Grommet Button](https://v2.grommet.io/button)

## Example Usage

    import ListForm from 'components/ListForm';

    <ListForm labelWidth="90px">
        <ListForm.Input label="Full Name" htmlFor="full_name" placeholder="Enter Full Name" defaultValue="Zerry Hogan"/>
        <ListForm.Input label="Cell Phone" htmlFor="cell_phone" placeholder="Enter Cell Phone" defaultValue="832-208-2797"/>
        <ListForm.Input label="Email" htmlFor="email" placeholder="Enter Email" defaultValue=""/>
        <ListForm.Button type="submit" label="Save"/>
    </ListForm>

## Links

-   [Link to Source](https://bitbucket.org/zerry-eztask/alegion-user-profile/src/master/src/components/ListForm/)

## Authors

-   **Zerry Hogan** - _Client_

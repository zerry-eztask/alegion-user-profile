import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Input from './components/Input';
import Button from './components/Button';
import Provider from './provider';

import './style.css';

/**
    * A form
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const ListForm = (props) => {

    const {children, editing, labelWidth, ...restProps} = props;

    const classConfig = {
        "list-form": true,
        "list-form--editing": editing
    };

    const className = classNames(classConfig);

    const value = {
        editing: editing,
        labelWidth: labelWidth
    };

    return (
        <Provider value={value}>
            <form className={className} {...restProps}>
                {children}
            </form>
        </Provider>
    );

};

ListForm.displayName = "ListForm";

ListForm.defaultProps = {
    editing: false
};

ListForm.propTypes = {
    labelWidth: PropTypes.string.isRequired,
    editing: PropTypes.bool
};

ListForm.Input = Input;
ListForm.Button = Button;

export default ListForm;

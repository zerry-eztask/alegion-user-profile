import React from 'react';
import ReadMe from './README.md';
import ListForm from './index';
import {storiesOf} from '@storybook/react';
import {withReadme} from 'storybook-readme';
import {action} from '@storybook/addon-actions';

const story = storiesOf('Components | List Form', module);

story.addDecorator(withReadme(ReadMe));

story.add('Default', () => (
    <ListForm style={{
        width: 400,
        margin: "0 auto"
    }} labelWidth="90px">
        <ListForm.Input label="Full Name" htmlFor="full_name" placeholder="Enter Full Name" defaultValue="Zerry Hogan"/>
        <ListForm.Input label="Cell Phone" htmlFor="cell_phone" placeholder="Enter Cell Phone" defaultValue="832-208-2797"/>
        <ListForm.Input label="Email" htmlFor="email" placeholder="Enter Email" defaultValue=""/>
        <div style={{marginTop: 5}}>
            <ListForm.Button type="button" label="Cancel" margin="xsmall"/>
            <ListForm.Button type="submit" label="Save" margin="xsmall"/>
        </div>
    </ListForm>
));

story.add('Editing', () => (
    <ListForm editing={true} style={{
        width: 400,
        margin: "0 auto"
    }} labelWidth="90px">
        <ListForm.Input label="Full Name" htmlFor="full_name" placeholder="Enter Full Name" defaultValue="Zerry Hogan"/>
        <ListForm.Input label="Cell Phone" htmlFor="cell_phone" placeholder="Enter Cell Phone" defaultValue="832-208-2797"/>
        <ListForm.Input label="Email" htmlFor="email" placeholder="Enter Email" defaultValue=""/>
        <div style={{marginTop: 5}}>
            <ListForm.Button type="button" label="Cancel" margin="xsmall"/>
            <ListForm.Button type="submit" label="Save" margin="xsmall"/>
        </div>
    </ListForm>
));

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {withConsumer} from './../../consumer';
import {Box} from 'grommet';
import passwordBackground from 'assets/img/password.png';

/**
    * An input field.
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Input = (props) => {

    const {children, label, htmlFor, placeholder, context, ...restProps} = props;

    const {editing, labelWidth} = context;

    const classConfig = {
        "list-form__row": true,
        [`list-form__row--type-${restProps.type || "text"}`]: true
    };

    const className = classNames(classConfig);

    const value = restProps.value || restProps.defaultValue;

    const isEmpty = value ? false : true;

    const isPassword = restProps.type == "password";

    const spanProps = {
        "style": {
            backgroundImage: isPassword && `url(${passwordBackground})`
        },
        "title": isPassword ? null : value,
        "data-empty": isEmpty
    };

    return (
        <Box direction="row" align="center" justify="between" className={className}>
            <Box width={labelWidth}>
                <label htmlFor={htmlFor}>{label}</label>
            </Box>
            <Box flex={true}>
                {editing && <input name={htmlFor} placeholder={placeholder} {...restProps}/>}
                {!editing && <span {...spanProps}>{value || placeholder}</span>}
            </Box>
        </Box>
    );

};

Input.displayName = "ListForm.Input";

Input.defaultProps = {};

Input.propTypes = {
    label: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired
};

export default withConsumer(Input);

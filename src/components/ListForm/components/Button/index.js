import React from 'react';
import classNames from 'classnames';
import {Button as GrommetButton} from 'grommet';

/**
    * A row for the list form
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Button = (props) => {

    const classConfig = {
        "list-form__button": true
    };

    const className = classNames(classConfig);

    return (
        <GrommetButton {...props}/>
    );

};

Button.displayName = "ListForm.Button";

export default Button;

import React from 'react';

export const Context = React.createContext();

export default class Provider extends React.Component {
    render() {
        return (
            <Context.Provider value={this.props.value}>
                {this.props.children}
            </Context.Provider>
        );
    }
}

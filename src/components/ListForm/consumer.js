import React from 'react';
import {Context} from './provider';

class Consumer extends React.Component {
    render() {
        return (
            <Context.Consumer>
                {this.props.children}
            </Context.Consumer>
        );
    }
}

const withConsumer = (Component) => {
    return (props) => {
        return (
            <Consumer>
                {(context) => <Component {...props} context={context}/>}
            </Consumer>
        );
    };
}

export {
    withConsumer
};

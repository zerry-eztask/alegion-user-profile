import React from 'react';
import {Grommet} from 'grommet';
import theme from './theme';

/**
    * Global Grommet Theme
    *
    * @version 1.0.0
    * @author Zerry Hogan
*/

const Theme = (props) => {

    const {children} = props;

    return (
        <Grommet theme={theme}>
            {children}
        </Grommet>
    );

};

export default Theme;
